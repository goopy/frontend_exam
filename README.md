# フロントエンド採用試験について

#### 作業準備

1. Bitbucketで[ユーザを作成](https://id.atlassian.com/signup?application=bitbucket&continue=https%3A//bitbucket.org/account/signin/%3Foptintocst%3D1%26next%3D/%3Faidsignup%3D1)する
2. このリポジトリをフォークする（非公開リポジトリのチェックを外してください）
3. Gitpod ボタンを押して、IDEを起動する
4. 自分の名前のブランチに切り替えて作業開始してください　例：`feature/名前`

---

### 指定されたページデザインのコーディングを行ってください。  

* 時間配分について
    * 試験内容説明 ( 10min )
    * マークアップ + スタイリング ( 30min + 60min )
    * スクリプト実装 ( 70min )

---

#### HTML/CSS

* 環境について
    * ドキュメントルートは`/public/`です
    * emmetが使用できます
    * CSSはSCSSが使用できます(自動でdart-sassにより`/src/scss/style.scss`がコンパイルされ、`/assets/css/style.min.css`が出力されます)
    * JSはES2015での記述可(自動でwebpackにより`/src/js/app.js`がbabelトランスパイル、バンドルされ、`/assets/js/bundle.min.js`が出力されます)
    * jQuery使用可(webpackにてpluginとして定義済み)
    * Vue.js/React.jsはnpmパッケージとしてインストール済み
    * npmパッケージやgulpfile,webpack.configは必要に応じて変更可能です
    * 画像はスライス済みのものを`/assets/img/`に格納済みです
* 制限について
    * CSSフレームワーク使用可(bootstrap5インストール済み)
    * pugは使用しないこと
    * ブラウザ要件は現行のモダンブラウザ(chrome/firefox)で動作すること
    * webfontは[Google font](https://fonts.google.com/)を使用すること
* ページ仕様について
    * 文字コードはUTF-8
    * ブレイクポイントは900px
    * OGPは無視して構いません
    * ヘッダーは画面に固定してください。
    * サービス内容は最大幅を設けてください

---
 
#### スクリプト実装  

* スマホメニューの開閉を実装してください
    * ハンバーガーアイコンとクローズアイコンをクリックすることで開閉すること
    * メニューの表示ギミックはお好みで構いません
* ページ上の画像が読み込み中はページ全体を非表示にし、読み込みが終わったら表示するような実装をJSとCSSを使用して実装してください。
    * フェードイン 20ms
* サービスの内容を`/assets/json/services.json`からajaxで動的に表示する実装にしてください（同ドメイン内の実装でOKです）
    * ajaxはfetchAPIを使用可（axios,jQueryでも可）
    * 可能であればVue.jsもしくはReactでサービス内容読み込み部分を実装してみてください
